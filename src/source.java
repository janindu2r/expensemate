
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hominda Marasinghe
 */
public class source extends Validation {

    public int getRowC(JTable jt) {
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
//        tm.setRowCount(0);
        return tm.getRowCount();
    }

    public double getMinimum(JTable jt) {
//        SELECT MAX(column_name) FROM table_name
        double max = 999999999;
        int row = 0;
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            if (max >= Double.parseDouble(jt.getValueAt(i, 3) + "")) {
                max = Double.parseDouble(jt.getValueAt(i, 3) + "");
                row = i;
            }
        }
        return max;
    }

    public double getMax(JTable jt) {
//        SELECT MAX(column_name) FROM table_name
        double max = 0;
        int row = 0;
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            if (max <= Double.parseDouble(jt.getValueAt(i, 3) + "")) {
                max = Double.parseDouble(jt.getValueAt(i, 3) + "");
                row = i;
            }
        }
        return max;
    }

    public String getMaxDate(JTable jt) {
//        SELECT MAX(column_name) FROM table_name
        double max = 0;
        int row = 0;
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            if (max <= Double.parseDouble(jt.getValueAt(i, 3) + "")) {
                max = Double.parseDouble(jt.getValueAt(i, 3) + "");
                row = i;
            }
        }
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMMMM");
//        System.out.println(df.format());
        return jt.getValueAt(row, 0) + "";
    }

    public String getMinimumDate(JTable jt) {
//        SELECT MAX(column_name) FROM table_name
        double max = 999999999;
        int row = 0;
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            if (max >= Double.parseDouble(jt.getValueAt(i, 3) + "")) {
                max = Double.parseDouble(jt.getValueAt(i, 3) + "");
                row = i;
            }
        }
        return jt.getValueAt(row, 0) + "";
    }

    public String getAvarage(JTable jt) {
        double tot = 0;
        double row = (double) getRowC(jt);
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            tot += Double.parseDouble(tm.getValueAt(i, 3) + "");
        }
        return Validation.PriceCon((tot / row) + "");
    }

    public String getMaxToMain(JTable jt) {
        double max = 0;
        int row = 0;
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            if (max <= Double.parseDouble(jt.getValueAt(i, 2) + "")) {
                max = Double.parseDouble(jt.getValueAt(i, 2) + "");
                row = i;
            }
        }
        return Validation.PriceCon(max + "");
    }

    public String getAmmount(JTable jt) {
        double tot = 0;
        double row = (double) getRowC(jt);
        DefaultTableModel tm = (DefaultTableModel) jt.getModel();
        for (int i = 0; i < jt.getRowCount(); i++) {
            tot += Double.parseDouble(tm.getValueAt(i, 3) + "");
        }
        return Validation.PriceCon((tot) + "");
    }

    public void monthVivewTable(JTable jt ,String s) {
        try {
            ResultSet rset = new JDBC().getData("select * from expense where expense_date_string='"+s+"' order by expense_date_string desc");
            DefaultTableModel tm = (DefaultTableModel) jt.getModel();
            tm.setRowCount(0);

            while (rset.next()) {
                Vector v = new Vector();
                v.add(rset.getString(2));
                v.add(rset.getString(5));
                v.add(rset.getString(4));
                v.add(rset.getString(3));
                // v.add(rset.getString(8));
                tm.addRow(v);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadTableMain(JTable jt) {
        try {
            ResultSet rset = new JDBC().getData("SELECT * FROM expense");
            ResultSet rset2 = new JDBC().getData("SELECT DISTINCT expense_date_string FROM expense");
            ResultSet rset3 = new JDBC().getData("SELECT DISTINCT expense_date_string FROM expense");

            DefaultTableModel tm = (DefaultTableModel) jt.getModel();
            tm.setRowCount(0);
            Vector vDate = new Vector();
            Vector vAmmount = new Vector();

            while (rset.next()) {
                vDate.add(rset.getString(6));
                vAmmount.add(rset.getDouble(3));
                System.out.println(rset.getDouble(3));
            }
            Vector distinctDate = new Vector();

            while (rset2.next()) {
                distinctDate.add(rset2.getString(1));
//                System.out.println(rset2.getString(1));
            }

            Object disDate[] = distinctDate.toArray();
            Object Odate[] = vDate.toArray();
            Object OAmmount[] = vAmmount.toArray();

            double temp = 0;
            double ammouTot[] = new double[disDate.length];

            for (int i = 0; i < disDate.length; i++) {
                for (int j = 0; j < Odate.length; j++) {
                    if ((disDate[i] + "").equals(Odate[j] + "")) {
                        temp += Double.parseDouble(OAmmount[j] + "");
                    }
                }
                ammouTot[i] = temp;
                temp = 0;
            }
            int i = 0;
            while (rset3.next()) {
                Vector v = new Vector();
                v.add(rset3.getString(1));
                v.add(Validation.PriceCon(ammouTot[i] + ""));
                tm.addRow(v);
                i++;
            }
            i = 0;

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadTTTT(JTable jt) {
        try {
            ResultSet rset = new JDBC().getData("SELECT * FROM expense");
            ResultSet rset2 = new JDBC().getData("SELECT DISTINCT expense_date FROM expense");
            ResultSet rset3 = new JDBC().getData("SELECT SUM(amount where DISTINCT expense.) DISTINCT expense_date FROM expense");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
