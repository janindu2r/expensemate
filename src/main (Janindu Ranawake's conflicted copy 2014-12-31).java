
import java.sql.ResultSet;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hominda
 */
public class main extends javax.swing.JFrame {

    /**
     * Creates new form main
     */
    private source sou = new source();

    public main(String s) {
        initComponents();
//        jTable_Bay_item.setValueAt("asd", 0, 0);
        jLabel1.setText("Hi " + s);
        sou.loadTableMain(jTable_Bay_item);
        jTable_Bay_item.setRowSelectionInterval(0, 0);
        setall();
    }

    void setall() {
        jLabel_month.setText(jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0).toString().split("-")[1]);
        try {
            ResultSet rsetMax = new JDBC().getData("SELECT MAX(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
            ResultSet rsetMini = new JDBC().getData("SELECT MIN(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
            ResultSet rsetAvg = new JDBC().getData("SELECT Avg(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
            ResultSet rsetTot = new JDBC().getData("SELECT SUM(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
            ResultSet rsetCount = new JDBC().getData("SELECT * FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
            while (rsetMax.next() && rsetMini.next() && rsetAvg.next() && rsetTot.next()) {
                jLabel_max.setText(Validation.PriceCon(rsetMax.getString(1)) + " Rs");
                jLabel_mini.setText(Validation.PriceCon(rsetMini.getString(1)) + " Rs");
                jLabe_avera.setText(Validation.PriceCon(rsetAvg.getString(1)) + " Rs");
                jLabel_ammount.setText(Validation.PriceCon(rsetTot.getString(1)) + " Rs");
            }
            int i = 0;
            while (rsetCount.next()) {
                i++;
            }
            jLabel_count.setText(i + " expenses");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel_ammount = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabe_avera = new javax.swing.JLabel();
        jLabel_max = new javax.swing.JLabel();
        jLabel_mini = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel_month = new javax.swing.JLabel();
        jLabel_count = new javax.swing.JLabel();
        jScrollPane_itrm = new javax.swing.JScrollPane();
        jTable_Bay_item = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Expences");
        setResizable(false);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setMaximumSize(new java.awt.Dimension(935, 519));
        jPanel1.setMinimumSize(new java.awt.Dimension(935, 519));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(0, 153, 255));

        jPanel3.setBackground(new java.awt.Color(0, 102, 204));

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 153, 153));
        jLabel4.setText("Total Ammount:");

        jLabel_ammount.setFont(new java.awt.Font("Arial", 0, 28)); // NOI18N
        jLabel_ammount.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_ammount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel_ammount.setText("jLabel5");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                    .addContainerGap(46, Short.MAX_VALUE)
                    .addComponent(jLabel_ammount, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(50, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(jLabel_ammount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(21, 21, 21)))
        );

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Maximum:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Minimum:");

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Daily Average :");

        jLabe_avera.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabe_avera.setForeground(new java.awt.Color(255, 255, 255));
        jLabe_avera.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabe_avera.setText("jLabel5");

        jLabel_max.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel_max.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_max.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel_max.setText("jLabel5");

        jLabel_mini.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel_mini.setForeground(new java.awt.Color(255, 255, 255));
        jLabel_mini.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel_mini.setText("jLabel5");

        jPanel4.setBackground(new java.awt.Color(0, 102, 204));

        jLabel_month.setFont(new java.awt.Font("Arial", 0, 31)); // NOI18N
        jLabel_month.setForeground(new java.awt.Color(255, 215, 0));
        jLabel_month.setText("jLabel2");

        jLabel_count.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel_count.setForeground(new java.awt.Color(153, 153, 153));
        jLabel_count.setText("jLabel3");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_month, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel_count, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel_month, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel_count)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 48, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel_mini, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel_max, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabe_avera, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20))))
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel_max, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel_mini, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabe_avera, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addGap(64, 64, 64)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 55, 300, 500));

        jTable_Bay_item.setAutoCreateRowSorter(true);
        jTable_Bay_item.setBackground(new java.awt.Color(153, 204, 255));
        jTable_Bay_item.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jTable_Bay_item.setForeground(new java.awt.Color(255, 255, 255));
        jTable_Bay_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Month", "Ammount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_Bay_item.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        jTable_Bay_item.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTable_Bay_item.setFillsViewportHeight(true);
        jTable_Bay_item.setFocusCycleRoot(true);
        jTable_Bay_item.setFocusable(false);
        jTable_Bay_item.setGridColor(new java.awt.Color(51, 204, 255));
        jTable_Bay_item.setIntercellSpacing(new java.awt.Dimension(0, 3));
        jTable_Bay_item.setMaximumSize(new java.awt.Dimension(300, 225));
        jTable_Bay_item.setMinimumSize(new java.awt.Dimension(300, 225));
        jTable_Bay_item.setOpaque(false);
        jTable_Bay_item.setRowHeight(45);
        jTable_Bay_item.setSelectionBackground(new java.awt.Color(0, 102, 204));
        jTable_Bay_item.setShowHorizontalLines(false);
        jTable_Bay_item.setShowVerticalLines(false);
        jTable_Bay_item.getTableHeader().setReorderingAllowed(false);
        jTable_Bay_item.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable_Bay_itemMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable_Bay_itemMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTable_Bay_itemMouseReleased(evt);
            }
        });
        jScrollPane_itrm.setViewportView(jTable_Bay_item);
        if (jTable_Bay_item.getColumnModel().getColumnCount() > 0) {
            jTable_Bay_item.getColumnModel().getColumn(0).setResizable(false);
            jTable_Bay_item.getColumnModel().getColumn(0).setPreferredWidth(40);
            jTable_Bay_item.getColumnModel().getColumn(1).setResizable(false);
            jTable_Bay_item.getColumnModel().getColumn(1).setPreferredWidth(20);
        }

        jPanel1.add(jScrollPane_itrm, new org.netbeans.lib.awtextra.AbsoluteConstraints(315, 56, 610, 500));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/user.png"))); // NOI18N
        jLabel1.setText("jLabel1");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 11, 270, 33));

        jButton1.setText("Logout");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 11, -1, 33));

        jButton2.setText("+");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 10, -1, 33));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 935, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTable_Bay_itemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_Bay_itemMouseClicked
        //jLabel2.setBounds(getLocatio, WIDTH, WIDTH, WIDTH);
    }//GEN-LAST:event_jTable_Bay_itemMouseClicked

    private void jTable_Bay_itemMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_Bay_itemMouseReleased
        if (evt.isPopupTrigger()) {
//            jPopupMenu1.show(jTable_Bay_item, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jTable_Bay_itemMouseReleased

    private void jTable_Bay_itemMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_Bay_itemMousePressed
        if (evt.getClickCount() == 2) {
            new MonthView(jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "").setVisible(rootPaneCheckingEnabled);

        }
//        jLabel_count.setText(jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0).toString().split("-")[1]);
//        try {
//            ResultSet rsetMax = new JDBC().getData("SELECT MAX(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
//            ResultSet rsetMini = new JDBC().getData("SELECT MIN(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
//            ResultSet rsetAvg = new JDBC().getData("SELECT Avg(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
//            ResultSet rsetTot = new JDBC().getData("SELECT SUM(amount) FROM expense WHERE expense_date_string = '" + jTable_Bay_item.getValueAt(jTable_Bay_item.getSelectedRow(), 0) + "" + "';");
//            while (rsetMax.next() && rsetMini.next() && rsetAvg.next() && rsetTot.next()) {
//                jLabel_max.setText(Validation.PriceCon(rsetMax.getString(1)));
//                jLabel_mini.setText(Validation.PriceCon(rsetMini.getString(1)));
//                jLabe_avera.setText(Validation.PriceCon(rsetAvg.getString(1)));
//                jLabel_ammount.setText(Validation.PriceCon(rsetTot.getString(1)));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        setall();
    }//GEN-LAST:event_jTable_Bay_itemMousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
        new Login().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new AddExpens().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        this.revalidate();
        sou.loadTableMain(jTable_Bay_item);
        jTable_Bay_item.setRowSelectionInterval(0, 0);
        setall();
    }//GEN-LAST:event_formWindowGainedFocus

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new main("test").setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabe_avera;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel_ammount;
    private javax.swing.JLabel jLabel_count;
    private javax.swing.JLabel jLabel_max;
    private javax.swing.JLabel jLabel_mini;
    private javax.swing.JLabel jLabel_month;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane_itrm;
    public javax.swing.JTable jTable_Bay_item;
    // End of variables declaration//GEN-END:variables
}
